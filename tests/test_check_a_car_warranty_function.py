from homework import check_if_car_has_warranty


def test_if_car_from_2017_with_45000_mileage_has_warranty():
    assert check_if_car_has_warranty(2017, 45000) == False


def test_if_car_from_2019_with_7000_mileage_has_warranty():
    assert check_if_car_has_warranty(2019, 70000) == False


def test_if_car_from_2020_with_50000_mileage_has_warranty():
    assert check_if_car_has_warranty(2020, 50000) == True

def test_if_car_from_2020_with_50000_mileage_has_warranty():
    assert check_if_car_has_warranty(2020, 50000) == True
