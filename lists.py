def list_of_numbers(list):
    sorted_list = sorted(list, reverse=True)
    return sorted_list[-4:]




if __name__ == '__main__':
    movies = ["Psy", "wilki", "szczury", "lisy", "koty"]
    print(len(movies))
    print(movies[0])
    print(movies[-1])
    movies.append("Her")
    print(movies[-1])
    movies.insert(2, "Interstellar")
    print(movies)
    movies.append("Five o\'clock")
    print(movies)
    print(list_of_numbers([5, 9, 90, 876]))
