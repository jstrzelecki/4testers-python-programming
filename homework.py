
def check_if_car_has_warranty(car_manufactured_date: int, car_mileage_in_kilometers: int):
    if car_manufactured_date < 2019 or car_mileage_in_kilometers > 60000:
        return False
    else:
        return True


if __name__ == '__main__':
    print("Date: 2020, Kilometers: 30000", ", Result:", check_if_car_has_warranty(2020, 30000))
    print("Date: 2020, Kilometeres: 70000", ", Result:", check_if_car_has_warranty(2020, 70000))
    print("Date: 2016, Kilometeres: 30000", ", Result:", check_if_car_has_warranty(2016, 30000))
    print("Date: 2016, Kilometeres: 120000", ", Result:", check_if_car_has_warranty(2016, 120000))
