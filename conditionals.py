def check_if_normal_conditions(temp_in_celsius, pressure_in_hpa):
    if temp_in_celsius == 0 and pressure_in_hpa == 1013:
        return True
    else:
        return False

def calculate_grade_for_test_score(points):
    if points >= 90:
        return 5
    elif points >= 75:
        return 4
    elif points >= 50:
        return 3
    else:
        return 2


if __name__ == '__main__':
    print(check_if_normal_conditions(0, 1013))
    print(check_if_normal_conditions(0, 1014))
    print(check_if_normal_conditions(1, 1013))
    print(check_if_normal_conditions(1, 1014))
    print(calculate_grade_for_test_score(75))