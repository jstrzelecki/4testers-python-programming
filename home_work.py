def check_for_animal_vaccination_requirement(animal_kind: str, animal_age_in_years: int):
    if animal_kind == "dog" and animal_age_in_years in (3, 6, 9, 12, 15, 18, 21):
        return f"{animal_kind} is qualified for vaccination every 3 years"
    elif animal_kind == "cat" and animal_age_in_years in (2, 4, 6, 8, 10, 12, 14, 16, 18, 20):
        return f"{animal_kind} is qualified for vaccination every 2 years"
    elif animal_kind and animal_age_in_years <= 1:
        return f"{animal_kind} is qualified for vaccination within first year of life"
    else:
        return "this is not supported"


def player_dict(nick, type, exp_points):
    return {
        "nick": nick,
        "type": type,
        "exp_points": exp_points
    }


if __name__ == '__main__':
    print(check_for_animal_vaccination_requirement("cat", 6))
    # print(player_dict(f"The player {nick} is of type {warrior} and has {exp_points}"))



