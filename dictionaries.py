def set_person_data(email: str, phone: int, city, street):
    return {
        'contact': {
            'email': email,
            'phone': phone
        },
        'address': {
            'city': city,
            'street': street
        }
    }



if __name__ == '__main__':

    pet = {"name": "Jacek", "specimen": "animal", "age": 21,
         "weight": 34, "isMale": True, "meal": ["meat", "water"]}
    print(pet["weight"])
    pet["weight"] = 67
    del pet["isMale"]
    print(pet)
    pet["meal"].append("snacks")
    print(pet)