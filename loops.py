import random
def print_numbers_from_one_to_thirty_divisible_by_7():
    for i in range(1, 31):
        if i % 7 == 0:
            print(i)

def generate_list_of_10_random_number_from_1000_to_5000():
    numbers = []
    for i in range(10):
        numbers.append(random.randint(1000, 5000))
    return numbers

def convert_celsius_to_fahrenheit(temps_celsius):
    temps_fahrenheit = []
    for temps in temps_celsius:
        temps_fahrenheit.append((1.8 * temps) + 32)
    return temps_fahrenheit


if __name__ == '__main__':
    print_numbers_from_one_to_thirty_divisible_by_7()
    print(generate_list_of_10_random_number_from_1000_to_5000())
    print(convert_celsius_to_fahrenheit([10.3, 23.4, 15.8]))
