import random


def print_random_employee_dict():
    random_email_list = ["ania@gmail.com", "jacek@gmail.com", "Sławek@gmail.com", "bartek@gmail.com"]
    email_idx = random.randrange(len(random_email_list))
    email = random_email_list[email_idx]

    random_seniority_years_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    seniority_years_idx = random.randrange(len(random_seniority_years_list))
    seniority_years = random_seniority_years_list[seniority_years_idx]

    random_sex_list = [False, True]
    female_idx = random.randrange(len(random_sex_list))
    female = random_sex_list[female_idx]

    return {"email": email,
            "seniority_years": seniority_years,
            "female": female
            }


if __name__ == '__main__':
    print(print_random_employee_dict())
    print(print_random_employee_dict())
    print(print_random_employee_dict())

