friends_name = "Jacek"
friends_age = 36
friends_pet_number = 0
has_driving_license = True
friendship_years = 10.5

print("Name:", friends_name, sep="******")
print("Age:", friends_age, sep="\t\t")
print("Pet number:", friends_pet_number)
print("Has driving license:", has_driving_license)
print("Friendship years:", friendship_years)

print("-----------------------")

print("Name:", friends_name, "Age:", friends_age, sep="\n")

print("Name:", friends_name,
      "Age:", friends_age,
      sep="\n"
      )

name_surname = "Jacek Strzelecki"
email = "jstrzelecki88@gmail.com"
phone_number = "+444409329032"

print("Name:", name_surname, "\nEmail:", email, "\nphone:", phone_number)

bio = "Name:" + name_surname + "\nEmail:" + email + "\nphone:" + phone_number

print(bio)

print("bio_smarter")
bio_smarter = f"Name: {name_surname}\nemail: {email}"
print(bio_smarter)

bio_docstring = f"""
Name: {name_surname}
email: {email}"""

print(bio_docstring)
