def print_area_of_a_circle_with_radius(r):
    area_of_a_circle = 3.1415 * r ** 2
    print(area_of_a_circle)


def return_kwadrtat(number):
    result = number ** 2
    return result

def return_kwadrtatd(a, b, c):
    result = a * b * c
    return result

def convert_celsius_to_fahrenheit(temp_in_celsius):
    return temp_in_celsius * 9 / 5 + 32

def name_city(name, city):
    return f"Witaj {name},w {city}"


if __name__ == '__main__':
    print_area_of_a_circle_with_radius(5)
    print(return_kwadrtat(16))
    print(return_kwadrtatd(3, 5, 7))

    print(name_city("Jacek", "Poznań"))
